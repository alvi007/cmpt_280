import lib280.list.LinkedIterator280;
import lib280.list.LinkedList280;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;

public class A1Q1 {

    /**
     *
     * @param howMany items the list should contain
     * @return a list containing random returns a randomly generated array of Sack objects with length howMany
     */

    public static Sack[] generatePlunder(int howMany) {
        Random generator = new Random();
        Sack grain[] = new Sack[howMany];

        for (int i = 0; i < howMany; i++) {
            grain[i] = new Sack(
            Grain.values()[generator.nextInt(Grain.values().length)],
            generator.nextDouble() * 100);
        }

        return grain;
    }

    public static void main(String[] args) {

        Sack[] captainJacksPlunder = generatePlunder(30);

        //Create a linkedList
        // Over load it with the types in enum such that each list will store Sack objects with one and only one type of grain

        @SuppressWarnings("unchecked")
        LinkedList280<Sack>[] linkedListArray = new LinkedList280[Grain.values().length];

        //Overloading each list in the array with the random numbers
        for (int i=0; i<Grain.values().length; i++) {
            //Initializing each element of the array
            linkedListArray[i] = new LinkedList280<>();

            for (int j=0; j<captainJacksPlunder.length; j++) {
                if (Grain.values()[i] == captainJacksPlunder[j].getType()) {
                    linkedListArray[i].insert(new Sack(captainJacksPlunder[j].getType(), captainJacksPlunder[j].getWeight()));
                }
            }
        }

        //adding all the weight
        for (int i=0; i<linkedListArray.length; i++) {
            LinkedIterator280<Sack> sackListIterator = new LinkedIterator280<Sack>(linkedListArray[i]);
            double total = 0;
            String typeOfGrain = sackListIterator.item().getType().name();
            while(sackListIterator.itemExists()) {
                total += sackListIterator.item().getWeight();
                sackListIterator.goForth();
            }

            System.out.println("Jack plundered "+total+" pounds of "+typeOfGrain);
        }
    }
}
