import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class RadixSortMSD {

	// *************************************************************
	// TODO Write YOUR RADIX SORT HERE
	// *************************************************************
	/**
	 * The high level radix sort method.
	 *
	 * @param items array of strings that become sorted alphabetically after execution
	 */
	public static void radixSort(String[] items) {
		//find maximal length among all the items
		int maxLength = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i].length() > maxLength) {
				maxLength = items[i].length();
			}
		}

		sortByDigit(items, 27, 0, maxLength);
	}

	/**
	 * Recursive radix sort method. It firstly call itself with increased digit parameter and then perform counting sort by digit
	 *
	 * @param keys      array to sort
	 * @param r         radix. It needs to be alphabet size + 1 (0 if representing absence of letter)
	 * @param digit     digit we sort by
	 * @param maxLength max length of keys string. It needs to stop the recursion if there are no more digits to sort by
	 */
	public static void sortByDigit(String[] keys, int r, int digit, int maxLength) {
		//ArrayList<ArrayList<>> is used to store lists
		ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < r; i++) {
			list.add(new ArrayList<String>());
		}

		if (digit < maxLength - 1) {
			sortByDigit(keys, r, digit + 1, maxLength);
		}


		//Counting sort
		for (String key : keys) {
			int indexToAdd = 0;
			if (key.length() > digit) {//if the letter is presented
				indexToAdd = key.charAt(digit) - 'A' + 1;
			}
			list.get(indexToAdd).add(key);
		}

		int pointer = 0;
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.get(i).size(); j++) {
				String word = list.get(i).get(j);
				keys[pointer] = word;
				pointer++;
			}
		}
	}


	public static void main(String args[]) {

		// *************************************************************
		// Change the input file by changing the line below.
		// *************************************************************
		String inputFile = "RadixSortMSD-Template/words-235884.txt";

		// Initialize a scanner to read the input file.
		Scanner S = null;
		try {
			S = new Scanner(new File(inputFile));
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + inputFile + "was not found.");
			return;
		}

		// Read the first line of the file and convert it to an integer to see how many
		// words are in the file.
		int numWords = Integer.valueOf(S.nextLine());

		// Initialize an array large enough to store numWords words.
		String items[] = new String[numWords];

		// Read each word from the input file and store it in the next free element of
		// the items array.
		int j=0;
		while(S.hasNextLine()) {
			items[j++] = S.nextLine().toUpperCase();
		}
		S.close();
		System.out.println("Done reading " + numWords + " words.");


		// Test and time your radix sort.
		long startTime = System.nanoTime();

		// *************************************************************
		// TODO CALL YOUR RADIX SORT TO SORT THE 'items' ARRAY HERE
		radixSort(items);
		// *************************************************************

		long stopTime = System.nanoTime();

		// Uncomment this section if you want the sorted list to be printed to the console.
		// (Good idea for testing with words-basictest.txt; leave it commented out though
		// for testing files with more than 50 words).
		/*
		for(int i=0; i < items.length; i++) {
			System.out.println(items[i]);
		}
		*/

		// Print out how long the sort took in milliseconds.
		System.out.println("Sorted " + items.length + " strings in " + (stopTime-startTime)/1000000.0 + "ms");

	}

}
